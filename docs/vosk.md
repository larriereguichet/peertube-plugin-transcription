# Vosk

Vosk `Recognizer` accept  audio data in PCM 16-bit mono format

> PCM: Pulse-code modulation, the standard form of digital audio in computers
> 16-bit 
> mono


> https://superuser.com/questions/675342/convert-mp3-to-wav-using-ffmpeg-for-vbr/675647

## Update Typescript definition file

```
yarn generate-vosk-types
```
