import { cwd } from 'process';
import { resolve } from 'path';
import { ModelFactory, RecognizerFactory } from './Recognizer';
import { SubtitleGenerator } from './Subtitle/SubtitleGenerator';

// vosk.setLogLevel(-1);

(async () => {
  try {
    const recognizerFactory = new RecognizerFactory(new ModelFactory('data/models'));
    const subtitleGenerator = new SubtitleGenerator(recognizerFactory, console);

    subtitleGenerator.generateToFile({
      inputFilePath: resolve(cwd(), './data/videos/eye.mp4'),
      outputFilePath: resolve(cwd(), './data/captions/captions.srt'),
      language: 'en',
      sampleRate: 16000,
    });
  } catch (e) {
    console.error(e);
  }
})();
