import { LoggerInterface } from '../Model';
import { LanguageModelManager } from '../Recognizer';

export class SettingsController {
  private readonly logger: LoggerInterface;
  private languageModelManager: LanguageModelManager;

  constructor(logger: LoggerInterface, languageModelManager: LanguageModelManager) {
    this.logger = logger;
    this.languageModelManager = languageModelManager;
  }

  async handleChanges(settingsEntries: { [settingName: string]: string | boolean }) {
    try {
      await this.languageModelManager.configureFromSettingsEntries(settingsEntries);
    } catch (e) {
      this.logger.log({ level: 'error', message: e });
    }
  }
}
