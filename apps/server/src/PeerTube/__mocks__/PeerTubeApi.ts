import { PeerTubeApiInterface } from '../PeerTubeApiInterface';

export const peerTubeApi: PeerTubeApiInterface = {
  getConfigAbout: jest.fn(),
  addVideoCaption: jest.fn(),
  getInstanceLanguage: jest.fn(() => Promise.resolve('fr')),
};
