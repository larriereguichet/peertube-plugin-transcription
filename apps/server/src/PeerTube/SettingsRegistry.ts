import { PluginSettingsManager, PluginStorageManager, RegisterClientFormFieldOptions } from '@peertube/peertube-types';
import { RegisterServerOptions } from '@peertube/peertube-types/server/types';
import { LoggerInterface } from '../Model';
import { SettingsRegistryInterface } from './SettingsRegistryInterface';

export type AllowedSettingType =
  | 'input'
  | 'input-checkbox'
  | 'input-password'
  | 'input-textarea'
  | 'markdown-text'
  | 'markdown-enhanced'
  | 'select'
  | 'html';

export interface SettingInterface extends Omit<RegisterClientFormFieldOptions, 'hidden'> {
  value?: string;
  private: boolean;
}

export interface SettingEntry {
  [settingName: string]: string | boolean;
}

export class Setting implements SettingInterface {
  default?: string | boolean;
  descriptionHTML?: string;
  html?: string;
  label?: string;
  name: string;
  options?: { value: string; label: string }[];
  type: AllowedSettingType;
  value?: string;
  private: boolean;

  constructor({
    defaultValue,
    descriptionHTML,
    html,
    label,
    name,
    options,
    type,
    value,
  }: SettingInterface & { defaultValue?: string | boolean; name: string }) {
    this.type = type;
    this.default = defaultValue;
    this.descriptionHTML = descriptionHTML;
    this.html = html;
    this.label = label;
    this.name = name;
    this.options = options;
    this.type = type || 'input';
    this.private = false;
    this.value = value;
  }
}

export class SettingsRegistry implements SettingsRegistryInterface {
  PLUGIN_SETTING_STORAGE_KEY_PREFIX = 'plugin_setting_';

  private pluginSettingsManager: PluginSettingsManager;
  private readonly registerPluginSetting: RegisterServerOptions['registerSetting'];
  private storageManager: PluginStorageManager;
  private logger: LoggerInterface;

  constructor(
    pluginSettingsManager: PluginSettingsManager,
    registerPluginSetting: RegisterServerOptions['registerSetting'],
    storageManager: PluginStorageManager,
    logger: LoggerInterface
  ) {
    this.pluginSettingsManager = pluginSettingsManager;
    this.registerPluginSetting = registerPluginSetting;
    this.storageManager = storageManager;
    this.logger = logger;
  }

  get(key: string) {
    return this.pluginSettingsManager.getSetting(key) as Promise<string | undefined>;
  }

  getPrevious(key: string) {
    return this.retrieveSettingValueFromStorage(key);
  }

  async set(key: string, value: string) {
    this.logger.debug(`Set plugin setting "${key}" value to "${value}".`);

    // This is kind of absurd, but here we can't call PeerTube `setSetting` or value will be bogus.

    await this.persist(key, value);
  }

  async register(setting: Setting) {
    this.logger.info(`Register plugin setting "${setting.name}" as "${setting.type}"`);

    // This has no effect when called outside the main plugin `register` function.
    await this.registerPluginSetting(setting);

    if (setting.value) {
      await this.set(setting.name, setting.value);
    }
  }

  getNameFromStorageKey(storageKey: string) {
    return storageKey.replace(this.PLUGIN_SETTING_STORAGE_KEY_PREFIX, '');
  }

  getStorageKey(key: string) {
    return `${this.PLUGIN_SETTING_STORAGE_KEY_PREFIX}${key}`;
  }

  private async persist(key: string, value: any) {
    await this.storageManager.storeData(this.getStorageKey(key), value);
  }

  public async retrieveSettingValueFromStorage(key: string): Promise<any | undefined> {
    return this.storageManager.getData(this.getStorageKey(key));
  }
}
