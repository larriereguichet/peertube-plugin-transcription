import { PluginStorageManager } from '@peertube/peertube-types';

const storage: Record<string, any> = {};

export const pluginStorageManager: PluginStorageManager = {
  storeData: jest.fn((key: string, value) => {
    storage[key] = value;
    return Promise.resolve();
  }),
  getData: jest.fn((key: string) => {
    try {
      return JSON.parse(storage[key]);
    } catch {
      return storage[key];
    }
  }),
};
