import { TranscriptionRequestInterface } from './TranscriptionRequestInterface';
import { MVideo } from '@peertube/peertube-types/server/types/models/video/video';
import { VideoFileInterface } from '../PeerTube/VideoFileInterface';

export class TranscriptionRequest implements TranscriptionRequestInterface {
  public language: string;
  public filePath: string;
  public uuid: string;

  constructor({ uuid, language }: Pick<MVideo, 'id' | 'uuid' | 'language'>, { path }: VideoFileInterface) {
    this.language = language;
    this.filePath = path;
    this.uuid = uuid;
  }
}
