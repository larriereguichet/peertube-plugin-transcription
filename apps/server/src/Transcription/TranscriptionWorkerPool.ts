import { AsyncResource } from 'async_hooks';
import { EventEmitter } from 'events';
import { resolve } from 'path';
import { Worker, setEnvironmentData } from 'worker_threads';
import { TranscriptionRequestInterface } from './TranscriptionRequestInterface';
import { LoggerInterface } from '../Model';
import { PerformanceObserver, performance } from 'perf_hooks';
import { TerminationRequest, TerminationSignal } from './TerminationRequest';

export const kWorkerFreedEvent = Symbol('kWorkerFreedEvent');

export type TranscriptionCallback = (e: Error | null, outputFilePath?: string | null) => void;

export interface TaskWithCallback {
  task: TranscriptionRequestInterface;
  callback: TranscriptionCallback;
}

export class WorkerPoolTaskInfo extends AsyncResource {
  private readonly callback: TranscriptionCallback;
  private readonly logger: LoggerInterface;

  constructor(callback: TranscriptionCallback, logger: LoggerInterface) {
    super('WorkerPoolTaskInfo');

    this.callback = callback;
    this.logger = logger;

    this.createPerformanceMark();
  }

  public done(err: Error | null, outputFilePath?: string | null) {
    this.measurePerformanceMark();
    this.runInAsyncScope(this.callback, null, err, outputFilePath);
    this.emitDestroy();
  }

  createPerformanceMark() {
    this.logger.info(`Created mark ${this.getStartPerformanceMarkName()}.`);
    performance.mark(this.getStartPerformanceMarkName());
  }

  measurePerformanceMark() {
    try {
      performance.mark(this.getEndPerformanceMarkName());
      performance.measure(
        this.asyncId().toString(),
        this.getStartPerformanceMarkName(),
        this.getEndPerformanceMarkName()
      );
    } catch (e) {
      this.logger.log({ level: 'error', message: e });
    }
  }

  getStartPerformanceMarkName() {
    return `${this.asyncId()}-started`;
  }

  getEndPerformanceMarkName() {
    return `${this.asyncId()}-ended`;
  }
}

export class TranscriptionWorkerPool extends EventEmitter {
  private numThreads: number;
  private workers: Worker[];
  private freeWorkers: Worker[];
  private tasks: TaskWithCallback[];
  private readonly dataPath: string;
  private logger: LoggerInterface;
  private resources: Map<number, WorkerPoolTaskInfo>;
  private readonly performanceObserver: PerformanceObserver | undefined;

  constructor(
    numThreads: number,
    dataPath: string,
    logger: LoggerInterface,
    performanceObserver?: PerformanceObserver
  ) {
    super();
    this.numThreads = numThreads;
    this.workers = [];
    this.freeWorkers = [];
    this.tasks = [];
    this.dataPath = dataPath;
    this.logger = logger;
    this.resources = new Map<number, WorkerPoolTaskInfo>();
    this.performanceObserver = performanceObserver;

    if (this.performanceObserver) {
      this.performanceObserver.observe({ type: 'measure' });
    }

    setEnvironmentData('dataPath', this.dataPath);

    for (let i = 0; i < numThreads; i++) {
      this.addNewWorker();
    }

    // Any time the kWorkerFreedEvent is emitted,
    // dispatch the next task pending in the queue, if any.
    this.on(kWorkerFreedEvent, this.onWorkerFreed.bind(this));

    this.addSignalEventHandler('SIGTERM');
    this.addSignalEventHandler('SIGINT');
  }

  addSignalEventHandler(signal: TerminationSignal) {
    process.on(signal, () => {
      this.logger.info(`Main thread received ${signal} signal.`);
      this.workers.forEach((worker) => worker.postMessage(new TerminationRequest(signal)));
      this.close();
    });
  }

  addNewWorker() {
    const worker = new Worker(
      resolve(
        process.env.JEST_WORKER_ID ? 'dist/server/Transcription' : __dirname,
        '..',
        'processTranscriptionRequest.js'
      )
    );
    worker.on('message', (result) => {
      // In case of success: call the callback that was passed to `runTask` and mark the Worker as free again.
      const resource = this.resources.get(worker.threadId);
      if (resource) {
        resource.done(null, result);
        this.resources.delete(worker.threadId);
      }

      this.freeWorkers.push(worker);
      this.emit(kWorkerFreedEvent);
    });
    worker.on('error', (e) => {
      this.logger.error(e.message, e);
      // In case of an uncaught exception: call the callback that was passed to `runTask` with the error.
      const resource = this.resources.get(worker.threadId);
      if (resource) {
        resource.done(e, null);
        this.resources.delete(worker.threadId);
      } else {
        this.emit('error', e);
      }
      // Remove the worker from the list and start a new Worker to replace the current one.
      this.workers.splice(this.workers.indexOf(worker), 1);
      this.addNewWorker();
    });

    worker.on('online', () => this.logger.info(`Worker n°${worker.threadId} is up!`));
    worker.on('exit', (exitCode) => this.logger.info(`Worker n°${worker.threadId} exited with code ${exitCode}...`));

    this.workers.push(worker);
    this.freeWorkers.push(worker);
    this.emit(kWorkerFreedEvent);
  }

  runTask({ task, callback }: TaskWithCallback) {
    if (this.freeWorkers.length === 0) {
      // No free threads, wait until a worker thread becomes free.
      this.tasks.push({ task, callback });
      return;
    }

    const worker = this.freeWorkers.pop();
    if (!worker) {
      return;
    }

    this.resources.set(worker.threadId, new WorkerPoolTaskInfo(callback, this.logger));
    worker.postMessage(task);
  }

  onWorkerFreed() {
    const taskWithCallback = this.tasks.shift();
    if (taskWithCallback) {
      this.runTask(taskWithCallback);
    }
  }

  close() {
    this.workers.forEach((worker) => worker.terminate());
  }
}
