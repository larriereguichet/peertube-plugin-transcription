import { PluginStorageManager } from '@peertube/peertube-types';
import { LoggerInterface } from '../Model';

export abstract class AbstractRepository {
  protected storage: PluginStorageManager;
  protected logger: LoggerInterface;

  constructor(storage: PluginStorageManager, logger: LoggerInterface) {
    this.storage = storage;
    this.logger = logger;
  }

  protected async getStorageData<E extends any>(key: string, defaultValue: E): Promise<E> {
    const data = (await this.storage.getData(key)) as string;
    this.logger.debug(`Retrieved ${typeof data} from storage key ${key}`, { data });

    if (data) {
      if (typeof data === 'object') {
        return data as E;
      }
      return JSON.parse(data);
    }

    return defaultValue;
  }
}
