import { PluginSettingsManager } from '@peertube/peertube-types';
import { SettingEntry } from '../PeerTube';

export type PluginSettingsManagerInterface = PluginSettingsManager & {
  onSettingsChange: (cb: (settings: SettingEntry) => Promise<any>) => void;
};
